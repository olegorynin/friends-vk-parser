import requests as r
MY_ID = 274773666
V = 5.52
TOKEN = 'dcba202edcba202edcba202ef8dccd363dddcbadcba202ebcd8e1fa5d81e7574d6829ba'


def generate_url_vk(method, params):
    gen_url = f'https://api.vk.com/method/{method}?v={V}&access_token={TOKEN}'
    for param_name in params.keys():
        gen_url += f'&{param_name}={params[param_name]}'
    return gen_url


def parse():
    first = r.get(generate_url_vk('friends.get', {'user_id':  MY_ID}))
    my_friends_list = first.json()['response']['items']
    mutual_friends = []
    for my_friend in my_friends_list:
        other = r.get(generate_url_vk('friends.get', {'user_id': my_friend}))
        if 'error' in other.json():
            continue
        list_of_friend = other.json()['response']['items']
        for friends_of_friend in list_of_friend:
            if friends_of_friend not in mutual_friends and friends_of_friend in my_friends_list:
                mutual_friends.append(friends_of_friend)
    return mutual_friends

if __name__ == '__main__':
    print(parse())


